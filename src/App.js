import styles from "./App.module.css"

function App() {

  return (
    <div className={styles.main}>
      <span className={styles.title}>Konrad</span>
    </div>
  );
}

export default App;
